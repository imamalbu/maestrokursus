<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Beranda extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_instruktur', 'instruktur');
        $this->load->model('M_paketkursus', 'paketkursus');
        $this->load->model('M_tempatkursus', 'tempatkursus');
        $this->load->model('M_daftarpeserta', 'daftarpeserta');
    }

    public function index()
    {
        $data['title'] = 'Beranda';
        $data['nav'] = 'home';
        $a_model = [];
        $a_model[] = ['model' => 'daftarpeserta'];
        $a_model[] = ['model' => 'paketkursus'];
        $a_model[] = ['model' => 'instruktur'];
        $a_data = [];
        foreach ($a_model as $val) {
            $model = $val['model'];
            $a_data[$model] = $this->$model->get()->num_rows();
        }
        $data['instruktur'] = $this->instruktur->getInstruktur(3)->result_array();
        $data['paketkursus'] = $this->paketkursus->getPaketBeranda()->result_array();
        $data['a_data'] = $a_data;
        $data['a_lowongan'] = $this->db->get_where('konten', ['kode_konten' => 'lowongan_kerja'])->result_array();
        $data['a_event'] = $this->db->get_where('konten', ['kode_konten' => 'event_terdekat'])->result_array();
        $this->load->view('client/inc/inc_header', $data);
        $this->load->view('client/inc/inc_navbar', $data);
        $this->load->view('client/home/index', $data);
        $this->load->view('client/inc/inc_footer', $data);
    }
}
