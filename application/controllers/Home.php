<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends MY_Controller
{
    public $user;

    function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('username')) {
            redirect('auth');
        }

        // var_dump(json_encode(getMenu(1)));
        // die;

        $this->load->model('M_paketkursus', 'paketkursus');
        $this->load->model('M_tempatkursus', 'tempatkursus');
        $this->load->model('M_instruktur', 'instruktur');
        $this->load->model('M_daftarpeserta', 'peserta');

        $this->user = $this->M_user->getBy(['username' => $this->session->userdata['username']])->row_array();
        $this->load->library('PDF');
    }

    public function index()
    {
        $this->breadcrumb->append_crumb('<i class="fa fa-home"></i> Beranda', site_url());
        $this->breadcrumb->append_crumb('Dashboard', site_url('home'));
        $id_owner = $this->session->userdata('idowner');

        if (!empty($id_owner)) {
            $a_tempat = getCourse()->row_array();
        }

        $paketkursus = empty($id_owner) ? $this->paketkursus->get()->num_rows() : $this->paketkursus->getBy(['idtempatkursus' => $a_tempat['idtempatkursus']])->num_rows();
        $instruktur = empty($id_owner) ? $this->instruktur->get()->num_rows() : $this->instruktur->getBy(['idtempatkursus' => $a_tempat['idtempatkursus']])->num_rows();
        $peserta = empty($id_owner) ? $this->peserta->get()->num_rows() : $this->peserta->getBy(['idtempatkursus' => $a_tempat['idtempatkursus']])->num_rows();
        $tempatkursus = $this->tempatkursus->get()->num_rows();

        $a_info = [];
        $a_info[] = ['label' => 'Paket Kursus', 'icon' => 'fas fa-book', 'data' => $paketkursus, 'color' => 'success'];
        $a_info[] = ['label' => 'Instruktur', 'icon' => 'fas fa-user-friends', 'data' => $instruktur, 'color' => 'secondary'];
        $a_info[] = ['label' => 'Peserta', 'icon' => 'fas fa-users', 'data' => $peserta, 'color' => 'warning'];
        $a_info[] = ['label' => 'Tempat Kursus', 'icon' => 'fas fa-school', 'data' => $tempatkursus, 'color' => 'success', 'role' => 1];

        $data['title'] = 'Dashboard';
        $data['profile'] = 'My Profile';
        $data['user'] = $this->user;
        $data['info'] = $a_info;
        $this->template->load('template', 'home/beranda', $data);
    }
}
