<?php


class Info extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_konten', 'konten');
        $this->load->model('M_jeniskonten', 'jeniskonten');
    }

    public function index($id = null)
    {
        $data['title'] = 'Kursus';
        $data['nav'] = 'info';
        $data['active'] = 'all';
        $data['users'] = $this->user;
        $data['navbar'] = $this->jeniskonten->get()->result_array();
        $data['a_konten'] = $this->konten->getReference()->result_array();
        if (!empty($id)) {
            $data['active'] = $id;
            $data['a_konten'] = $this->konten->getReferences($id)->result_array();
        }
        $this->load->view('client/inc/inc_header', $data);
        $this->load->view('client/inc/inc_navbar', $data);
        $this->load->view('client/info/index', $data);
        $this->load->view('client/inc/inc_footer', $data);
    }

    public function detail($key)
    {
        $data['title'] = 'Detail Info';
        $data['nav'] = 'info';
        $data['a_info'] = $this->konten->getReference($key)->row_array();
        $this->load->view('client/inc/inc_header', $data);
        $this->load->view('client/inc/inc_navbar', $data);
        $this->load->view('client/info/detail', $data);
        $this->load->view('client/inc/inc_footer', $data);
    }
}
