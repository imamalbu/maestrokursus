<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Datapeserta extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('username')) {
            redirect('auth');
        }

        //set default
        $this->title = 'Data Peserta';
        $this->menu = 'datapeserta';
        $this->parent = 'masters';
        $this->pager = true;
        $this->setKolom();
    }

    public function setKolom()
    {
        $a_jk = [
            'L' => 'Laki-Laki',
            'P' => 'Perempuan'
        ];

        $a_kolom = [];
        $a_kolom[] = ['kolom' => ':no', 'label' => 'No', 'is_null' => true];
        $a_kolom[] = ['kolom' => 'namapeserta', 'label' => 'Nama'];
        $a_kolom[] = ['kolom' => 'jeniskelamin', 'label' => 'Jenis Kelamin', 'type' => 'S', 'option' => $a_jk];
        $a_kolom[] = ['kolom' => 'email', 'label' => 'Email'];
        $a_kolom[] = ['kolom' => 'notelp', 'label' => 'Telp', 'type' => 'N'];
        $a_kolom[] = ['kolom' => 'alamat', 'label' => 'Alamat', 'type' => 'A'];

        $this->a_kolom = $a_kolom;
    }
}
