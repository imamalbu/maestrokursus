<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Instruktur extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('username')) {
            redirect('auth');
        }

        $this->load->model('M_tempatkursus', 'tempatkursus');
        //set default
        $this->title = 'Data Instruktur';
        $this->menu = 'instruktur';
        $this->parent = 'masters';
        $this->pager = true;
        $this->setKolom();
    }

    public function setKolom()
    {
        $a_jk = [
            'L' => 'Laki-Laki',
            'P' => 'Perempuan'
        ];


        if (!empty($this->session->userdata('idowner'))) {
            $key = getCourse()->row_array()['idtempatkursus'];
            $this->cond = ['idtempatkursus' => $key];
            $a_data = $this->tempatkursus->getListCombo(['idtempatkursus' => $key]);
        } else {
            $a_data = $this->tempatkursus->getListCombo();
        }

        $a_kolom = [];
        $a_kolom[] = ['kolom' => ':no', 'label' => 'No', 'is_null' => true];
        $a_kolom[] = ['kolom' => 'namainstruktur', 'label' => 'Nama'];
        $a_kolom[] = ['kolom' => 'jeniskelamin', 'label' => 'Jenis Kelamin', 'type' => 'S', 'option' => $a_jk];
        $a_kolom[] = ['kolom' => 'pendidikan', 'label' => 'Pendidikan'];
        $a_kolom[] = ['kolom' => 'lulusan', 'label' => 'Lulusan'];
        $a_kolom[] = ['kolom' => 'notelp', 'label' => 'Telp'];
        $a_kolom[] = ['kolom' => 'idtempatkursus', 'label' => 'Tempat Kursus', 'type' => 'S', 'option' => $a_data];
        $a_kolom[] = ['kolom' => 'foto', 'label' => 'Foto', 'type' => 'F', 'path' => './assets/img/instruktur/', 'file_type' => 'jpg|png|jpeg', 'is_tampil' => false];

        $this->a_kolom = $a_kolom;
    }
}
