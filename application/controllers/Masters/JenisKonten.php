<?php
defined('BASEPATH') or exit('No direct script access allowed');

class JenisKonten extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('username')) {
            redirect('auth');
        }

        $this->load->model('M_jeniskonten', 'jeniskonten');

        //set default
        $this->title = 'Jenis Konten';
        $this->menu = 'jeniskonten';
        $this->parent = 'masters';
        $this->pager = true;
        $this->setKolom();
    }

    public function setKolom()
    {
        $a_kolom = [];
        $a_kolom[] = ['kolom' => ':no', 'label' => 'No', 'is_null' => true];
        $a_kolom[] = ['kolom' => 'jeniskonten', 'label' => 'Jenis Konten'];
        $a_kolom[] = ['kolom' => 'info_tambahan', 'label' => 'Info Tambahan', 'type' => 'A', 'is_null' => true];

        $this->a_kolom = $a_kolom;
    }
}
