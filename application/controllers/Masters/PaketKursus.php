<?php
defined('BASEPATH') or exit('No direct script access allowed');

class PaketKursus extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('username')) {
            redirect('auth');
        }

        $this->load->model('M_tempatkursus', 'tempatkursus');
        $this->load->model('M_jeniskursus', 'jenis');
        //set default
        $this->title = 'Data Paket Kursus';
        $this->menu = 'paketkursus';
        $this->parent = 'masters';
        $this->pager = true;
        $this->setKolom();
    }

    public function setKolom()
    {

        if (!empty($this->session->userdata('idowner'))) {
            $key = getCourse()->row_array()['idtempatkursus'];
            $this->cond = ['idtempatkursus' => $key];
            $a_data = $this->tempatkursus->getListCombo(['idtempatkursus' => $key]);
        } else {
            $a_data = $this->tempatkursus->getListCombo();
        }

        $a_jenis = $this->jenis->getListCombo();

        $a_kolom = [];
        $a_kolom[] = ['kolom' => ':no', 'label' => 'No', 'is_null' => true];
        $a_kolom[] = ['kolom' => 'namapaketkursus', 'label' => 'Nama Paket'];
        $a_kolom[] = ['kolom' => 'idjeniskursus', 'label' => 'Jenis Kursus', 'type' => 'S', 'option' => $a_jenis];
        $a_kolom[] = ['kolom' => 'idtempatkursus', 'label' => 'Tempat Kursus', 'type' => 'S', 'option' => $a_data];
        $a_kolom[] = ['kolom' => 'durasi', 'label' => 'Durasi'];
        $a_kolom[] = ['kolom' => 'kuota', 'label' => 'Kuota', 'type' => 'N'];
        $a_kolom[] = ['kolom' => 'harga', 'label' => 'Harga', 'set_currency' => true];
        $a_kolom[] = ['kolom' => 'jadwal', 'label' => 'Jadwal', 'type' => 'F', 'path' => './assets/doc/datapaket/', 'file_type' => 'pdf|docx|xls'];
        $a_kolom[] = ['kolom' => 'silabus', 'label' => 'Silabus', 'type' => 'F', 'path' => './assets/doc/datapaket/', 'file_type' => 'pdf|docx|xls'];
        $a_kolom[] = ['kolom' => 'foto', 'label' => 'Foto', 'type' => 'F', 'path' => './assets/doc/datapaket/', 'file_type' => 'jpg|png|jpeg', 'is_tampil' => false];
        $a_kolom[] = ['kolom' => 'deskripsi', 'label' => 'Deskripsi', 'type' => 'A', 'is_tampil' => false];

        $this->a_kolom = $a_kolom;
    }
}
