<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Menu extends CI_Controller
{
    public $user;

    public function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('username')) {
            redirect('auth');
        }

        //load model
        $this->load->model('M_user', 'users');
        $this->load->model('M_menu', 'menu');
        $this->load->model('M_submenu', 'submenu');
        $this->load->model('M_pengaturan', 'pengaturan');
        $this->load->model('M_role');
        $this->load->model('M_menu');
        $this->load->model('M_access_menu');

        $this->user = $this->users->getBy(['username' => $this->session->userdata['username']])->row_array();
    }

    public function index()
    {
        $this->breadcrumb->append_crumb('<i class="fa fa-home"></i> Beranda', site_url());
        $this->breadcrumb->append_crumb('Pengaturan', '#');
        $this->breadcrumb->append_crumb('Manajemen Menu', '#');

        $data['title'] = 'Manajemen Menu';
        $data['profile'] = 'My Profile';
        $data['user'] = $this->user;
        $data['menu'] = $this->db->get('user_menu')->result_array();

        $this->form_validation->set_rules('menu', 'Menu', 'required');

        if ($this->form_validation->run() == false) {
            $this->template->load('template', 'menu/index', $data);
        } else {
            if (!empty($_POST)) {
                $id = $this->input->post('key');
                $act = $this->input->post('act');
                $menu = $this->input->post('menu');
                $icon = $this->input->post('icon');

                $data = [
                    'id' => '',
                    'menu' => $menu,
                    'icon' => $icon,
                ];

                switch ($act) {
                    case 'simpan':
                        if (!empty($menu) && !empty($icon)) {
                            $insert = $this->menu->insert($data);
                            $msg = ' menambahkan menu';
                            $insert ? setMessage('Berhasil ' . $msg, 'success') : setMessage('Gagal ' . $msg, 'danger');
                        } else {
                            setMessage('Harap isi form dengan benar !', 'danger');
                        }
                        break;
                    case 'edit':
                        if (!empty($menu) && !empty($icon)) {
                            $update = $this->menu->update(['menu' => $menu, 'icon' => $icon], $id);
                            $msg = ' merubah menu';
                            $update ? setMessage('Berhasil ' . $msg, 'success') : setMessage('Gagal ' . $msg, 'danger');
                        } else {
                            setMessage('Harap isi form dengan benar !', 'danger');
                        }
                        break;
                }
            }
            redirect('menu');
        }
    }

    public function deleteMenu($id)
    {
        $query = "DELETE FROM user_menu WHERE id='$id'";
        $result = $this->db->query($query);

        $result ? setMessage('Berhasil menghapus menu!', 'success') : setMessage('Gagal menghapus menu!', 'danger');
        redirect('menu');
    }

    public function getMenu($id)
    {
        $menu = $this->menu->getBy(['id' => $id])->row_array();

        echo json_encode($menu);
    }

    public function submenu()
    {
        $this->breadcrumb->append_crumb('<i class="fa fa-home"></i> Beranda', site_url());
        $this->breadcrumb->append_crumb('Pengaturan', '#');
        $this->breadcrumb->append_crumb('Manajemen Sub Menu', '#');

        $a_data = [];
        $a_data[0] = 'Semua Role';
        $val = $this->M_role->get()->result_array();

        foreach ($val as $value) {
            $a_data[$value['id']] = $value['role'];
        }

        $data['title'] = 'Submenu Management';
        $data['profile'] = 'My Profile';
        $data['user'] = $this->user;
        $data['submenu'] = $this->menu->getSubMenu();
        $data['menu'] = $this->db->get('user_menu')->result_array();
        $data['option_role'] = $a_data;

        $this->form_validation->set_rules('title', 'Title', 'required');
        $this->form_validation->set_rules('menu_id', 'Menu', 'required');
        $this->form_validation->set_rules('url', 'URL', 'required');



        if ($this->form_validation->run() == false) {
            $this->template->load('template', 'menu/submenu', $data);
        } else {
            if (!empty($_POST)) {
                $title = $this->input->post('title');
                $menuid = $this->input->post('menu_id');
                $url = $this->input->post('url');
                $role = $this->input->post('role');
                $isactive = $this->input->post('is_active');
                $act = $this->input->post('act');
                $key = $this->input->post('key');
                $id_role = $role != 0 ? $role : 0;

                $data = [
                    'title' => $title,
                    'menu_id' => $menuid,
                    'url' => $url,
                    'role_id' => $id_role,
                    'is_active' => $isactive
                ];

                switch ($act) {
                    case 'simpan':
                        $insert = $this->submenu->insert($data);
                        $msg = 'menambah submenu';
                        $insert ? setMessage('Berhasil ' . $msg, 'success') : setMessage('Gagal ' . $msg, 'danger');
                        break;
                    case 'edit':
                        $update = $this->submenu->update($data, $key);
                        $msg = 'merubah submenu';
                        $update ? setMessage('Berhasil ' . $msg, 'success') : setMessage('Gagal ' . $msg, 'danger');
                        break;
                }
            }


            redirect('menu/submenu');
        }
    }

    public function deleteSubmenu($id)
    {
        $delete = $this->submenu->delete($id);
        $delete ? setMessage('Berhasil menghapus submenu!', 'success') : setMessage('Gagal menghapus submenu!', 'danger');
        redirect('menu/submenu');
    }

    public function getSubmenu($id)
    {
        $submenu = $this->submenu->getBy(['id' => $id])->row_array();
        echo json_encode($submenu);
    }

    public function user()
    {
        $this->breadcrumb->append_crumb('<i class="fa fa-home"></i> Beranda', site_url());
        $this->breadcrumb->append_crumb('Pengaturan', '#');
        $this->breadcrumb->append_crumb('User', '#');

        $data['title'] = 'Manajemen User';
        $data['profile'] = 'Manajemen User';
        $data['user'] = $this->user;
        $data['role'] = $role = $this->db->get('user_role')->result_array();
        $a_role = array();
        foreach ($role as $row) {
            $a_role[$row['id']] = $row['role'];
        }
        $data['a_role'] = $a_role;

        //ambil inputan dari user
        if ($this->input->post('submit')) {
            $data['keyword'] = $this->input->post('keyword');
            $this->session->set_userdata('keyworduser', $data['keyword']);
        } else {
            $data['keyword'] = $this->session->userdata('keyworduser');
        }


        //config pagination sesuai searchnya
        $this->db->like('username', $data['keyword']);
        $this->db->from('users');
        $data['total_rows'] = $this->db->count_all_results();

        //set pagination
        $limit = 10;
        setPagination('user', 'menu/user', $limit, $data['total_rows']);
        $data['start'] = $this->uri->segment(3);
        $data['users'] = $this->users->getUsers($limit, $data['start'], $data['keyword']);

        $this->template->load('template', 'menu/manageusers', $data);

        if (!empty($_POST)) {
            $act = $this->input->post('act');
            $key = $this->input->post('key');
            $username = $this->input->post('username');
            $nama = $this->input->post('nama');
            $password = $this->input->post('password');
            $password2 = $this->input->post('password2');
            $email = $this->input->post('email');
            $role = $this->input->post('role');

            $checkUsername = $this->users->getBy(['username' => $username])->num_rows();
            $checkEmail = $this->users->getBy(['email' => $email])->num_rows();
            $rowUsername = $this->users->getBy(['username' => $username])->row_array();
            $rowEmail = $this->users->getBy(['email' => $email])->row_array();

            switch ($act) {
                case 'simpan':
                    if ($password != $password2) {
                        setMessage('Password tidak cocok', 'danger');
                    } else {
                        $data = [
                            'username' => $username,
                            'name' => $nama,
                            'email' => $email,
                            'image' => 'default.jpg',
                            'password' => password_hash($password, PASSWORD_DEFAULT),
                            'role_id' => $role,
                            'is_active' => 1,
                            'date_created' => time()
                        ];
                        if (
                            !empty($username) && !empty($nama) && !empty($password) &&
                            !empty($password2) && !empty($role)
                        ) {
                            if ($checkUsername < 1) {
                                if ($checkEmail < 1) {
                                    $insert = $this->users->insert($data);
                                    $msg = 'menambahkan user';
                                    $insert ? setMessage('Berhasil ' . $msg, 'success') : setMessage('Gagal ' . $msg, 'danger');
                                } else {
                                    setMessage('Email sudah digunakan!', 'danger');
                                }
                            } else {
                                setMessage('Username sudah digunakan', 'danger');
                            }
                        } else {
                            setMessage('Harap isi form dengan benar!', 'danger');
                        }
                    }
                    break;
                case 'edit':
                    $data = [
                        'username' => $username,
                        'name' => $nama,
                        'email' => $email,
                        'role_id' => $role,
                    ];

                    if (!empty($username) && !empty($nama) && !empty($role)) {
                        if ($rowUsername['id'] == $key || empty($rowUsername['id'])) {
                            if ($rowEmail['id'] == $key || empty($rowEmail['id'])) {
                                $update = $this->users->update($data, $key);
                                $msg = 'merubah user';
                                $update ? setMessage('Berhasil ' . $msg, 'success') : setMessage('Gagal ' . $msg, 'danger');
                            } else {
                                setMessage('Email sudah digunakan!', 'danger');
                            }
                        } else {
                            setMessage('Username sudah digunakan', 'danger');
                        }
                    } else {
                        setMessage('Harap isi form dengan benar!', 'danger');
                    }
                    break;
            }
            redirect('menu/user');
        }
    }

    public function getUser($id)
    {
        $get = $this->users->getBy(['id' => $id])->row_array();

        echo json_encode($get);
    }

    public function setStatusUser($id, $status)
    {
        if ($status == 1) {
            $data = [
                'is_active' => 0
            ];
            $set = $this->users->update($data, $id);
            $msg = 'menonaktifkan user';
            $set ? setMessage('Berhasil ' . $msg, 'success') : setMessage('Gagal ' . $msg, 'danger');
        } else {
            $data = [
                'is_active' => 1
            ];
            $set = $this->users->update($data, $id);
            $msg = 'mengaktifkan user';
            $set ? setMessage('Berhasil ' . $msg, 'success') : setMessage('Gagal ' . $msg, 'danger');
        }
        redirect('menu/user');
    }

    public function deleteUser($id)
    {
        $delete = $this->users->delete($id);
        $msg = 'menghapus user';
        $delete ? setMessage('Berhasil ' . $msg, 'success') : setMessage('Gagal ' . $msg, 'danger');
        redirect('menu/user');
    }

    public function resetPasswordUser()
    {
        $password = $this->input->post('password');
        $password2 = $this->input->post('password2');
        $key = $this->input->post('key');

        if (!empty($password) && !empty($password2)) {
            if ($password != $password2) {
                setMessage('Password tidak cocok', 'danger');
            } else {
                $hash = password_hash($password, PASSWORD_DEFAULT);
                $data = [
                    'password' => $hash
                ];
                $set = $this->users->update($data, $key);
                $msg = 'mereset password';
                $set ? setMessage('Berhasil ' . $msg, 'success') : setMessage('Gagal ' . $msg, 'danger');
            }
        } else {
            setMessage('Mohon isi password dengan benar!', 'danger');
        }

        redirect('menu/user');
    }

    public function pengaturan()
    {
        $data['title'] = 'Pengaturan Aplikasi';
        $data['profile'] = 'Pengaturan Aplikasi';
        $data['user'] = $this->user;
        $data['pengaturan'] = $this->pengaturan->get()->result_array();
        $this->template->load('template', 'menu/pengaturan', $data);

        if (!empty($_POST)) {
            redirect('menu/pengaturan');
        }
    }

    public function formPengaturan($id = 'tambah')
    {
        if ($id == 'tambah') {
            $title = 'Tambah Pengaturan';
            $key = '';
            $data['setting'] = $key;
        } else {
            $title = 'Edit Pengaturan';
            $data['pengaturan'] = $this->pengaturan->getBy(['idpengaturan' => $id])->row_array();
            $data['setting'] = $data['pengaturan'];
        }

        $data['title'] = $title;
        $data['profile'] = $title;

        $data['user'] = $this->user;
        $this->template->load('template', 'menu/formpengaturan', $data);

        if (!empty($_POST)) {
            $act = $this->input->post('act');
            $idpengaturan = $this->input->post('idpengaturan');
            $namapengaturan = $this->input->post('namapengaturan');
            $val = $this->input->post('val');

            $data = [
                'idpengaturan' => $idpengaturan,
                'namapengaturan' => $namapengaturan,
                'valuepengaturan' => $val
            ];


            switch ($act) {
                case 'simpan':
                    $insert = $this->pengaturan->insert($data);
                    $msg = 'menambahkan pengaturan';
                    $insert ? setMessage('Berhasil ' . $msg, 'success') : setMessage('Gagal ' . $msg, 'danger');
                    break;
                case 'edit':
                    $update = $this->pengaturan->update($data, $idpengaturan);
                    $msg = 'merubah pengaturan';
                    $update ? setMessage('Berhasil ' . $msg, 'success') : setMessage('Gagal ' . $msg, 'danger');
                    break;
            }
            redirect('menu/pengaturan');
        }
    }

    public function deletePengaturan($id)
    {
        $delete = $this->pengaturan->delete($id);
        $msg = 'menghapus pengaturan';
        $delete ? setMessage('Berhasil ' . $msg, 'success') : setMessage('Gagal ' . $msg, 'danger');

        redirect('menu/pengaturan');
    }

    public function role()
    {
        $this->breadcrumb->append_crumb('<i class="fa fa-home"></i> Beranda', site_url());
        $this->breadcrumb->append_crumb('Master', '#');
        $this->breadcrumb->append_crumb('Manajemen Role', '#');

        $data['title'] = 'Manajemen Role';
        $data['profile'] = 'Manajemen ROle';
        $data['user'] = $this->user;

        if (!empty($_POST)) {
            $key = $this->input->post('key');
            $act = $this->input->post('act');
            $role = $this->input->post('namarole');

            if ($act == "simpan") {
                if (!empty($role)) {
                    $rec = array();
                    $rec['role'] = $role;

                    $ok = $this->M_role->insert($rec);
                    $msg = 'menambahkan role';
                    $ok ? setMessage('Berhasil ' . $msg, 'success') : setMessage('Gagal ' . $msg, 'danger');
                    redirect('menu/role');
                } else {
                    setMessage('Harap isi form dengan benar', 'danger');
                }
            } else if ($act == "edit") {
                $rec = array();
                $rec['role'] = $role;

                $ok = $this->M_role->update($rec, $key);
                $msg = 'mengubah role';
                $ok ? setMessage('Berhasil ' . $msg, 'success') : setMessage('Gagal ' . $msg, 'danger');
                redirect('menu/role');
            }
        }

        $data['role'] = $this->M_role->get()->result_array();

        $this->template->load('template', 'menu/role', $data);
    }

    public function getRole($id)
    {
        $result = $this->M_role->get($id)->row_array();

        echo json_encode($result);
    }

    public function deleteRole()
    {
        $id = $this->input->post('key');
        $delete = $this->M_role->delete($id);
        $msg = 'menghapus role';
        $delete ? setMessage('Berhasil ' . $msg, 'success') : setMessage('Gagal ' . $msg, 'danger');
        redirect('menu/role');
    }

    public function roleAccess($id)
    {
        $this->breadcrumb->append_crumb('<i class="fa fa-home"></i> Beranda', site_url());
        $this->breadcrumb->append_crumb('Menu', '#');
        $this->breadcrumb->append_crumb('Role', site_url('menu/role'));
        $this->breadcrumb->append_crumb('Role Access', '#');

        $data['title'] = 'Role Access';
        $data['profile'] = 'Role Access';
        $data['user'] = $this->user;

        $data['role'] = $this->M_role->getBy(['id' => $id])->row_array();

        $data['menu'] = $this->M_menu->get()->result_array();
        $this->template->load('template', 'menu/role-access', $data);
    }

    public function changeAccess()
    {
        $menuId = $this->input->post('menuId');
        $roleId = $this->input->post('roleId');

        $data = [
            'role_id' => $roleId,
            'menu_id' => $menuId
        ];

        $result = $this->M_access_menu->getBy($data);

        if ($result->num_rows() < 1) {
            $this->M_access_menu->insert($data);
        } else {
            $this->M_access_menu->delete($result->row_array()['id']);
        }

        setMessage('Berhasil merubah akses!', 'success');
    }
}
