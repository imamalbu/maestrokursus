<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Konten extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('username')) {
            redirect('auth');
        }

        $this->load->model('M_jeniskonten', 'jeniskonten');

        //set default
        $this->title = 'Manajemen Konten';
        $this->menu = 'konten';
        $this->parent = 'pengaturan';
        $this->pager = true;
        $this->setKolom();
    }

    public function setKolom()
    {
        $a_jenis = $this->jeniskonten->getListCombo();

        $a_kolom = [];
        $a_kolom[] = ['kolom' => ':no', 'label' => 'No', 'is_null' => true];
        $a_kolom[] = ['kolom' => 'kode_konten', 'label' => 'Kode'];
        $a_kolom[] = ['kolom' => 'judul', 'label' => 'Judul'];
        $a_kolom[] = ['kolom' => 'subjudul', 'label' => 'Sub Judul', 'is_null' => true, 'is_tampil' => false];
        $a_kolom[] = ['kolom' => 'idjeniskonten', 'label' => 'Jenis Konten', 'type' => 'S', 'option' => $a_jenis];
        $a_kolom[] = ['kolom' => 'url', 'label' => 'URL (Optional)', 'is_tampil' => false, 'is_null' => true];
        $a_kolom[] = ['kolom' => 'deskripsi', 'label' => 'Deskripsi', 'type' => 'A', 'is_tampil' => false, 'is_tiny' => 'ok', 'is_null' => true];
        $a_kolom[] = ['kolom' => 'info_tambahan', 'label' => 'Info Tambahan', 'type' => 'A', 'is_tampil' => false, 'is_tiny' => 'ok', 'is_null' => true];
        $a_kolom[] = ['kolom' => 'jumbotron', 'label' => 'Jumbotron', 'type' => 'F', 'file_type' => 'jpg|png|jpeg', 'path' => './assets/img/konten/', 'is_null' => true];
        $a_kolom[] = ['kolom' => 'gambar', 'label' => 'Gambar', 'type' => 'F', 'file_type' => 'jpg|png|jpeg', 'path' => './assets/img/konten/'];

        $this->a_kolom = $a_kolom;
    }
}
