<?php
defined('BASEPATH') or exit('No direct script access allowed');

class DaftarPeserta extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('username')) {
            redirect('auth');
        }

        $query = "SELECT seenby_admin FROM daftar_peserta WHERE seenby_admin=0";
        $count = $this->db->query($query)->num_rows();

        if($count > 0){
            $this->db->where('seenby_admin', 0);
            $this->db->update('daftar_peserta', ['seenby_admin' => 1]);
        }

        $this->load->model('M_tempatkursus', 'tempatkursus');
        $this->load->model('M_paketkursus', 'paketkursus');
        $this->load->model('M_peserta', 'peserta');

        //set default
        $this->title = 'Daftar Peserta';
        $this->menu = 'daftarpeserta';
        $this->parent = 'peserta';
        $this->pager = true;
        $this->is_crud = 1;
        $this->setKolom();
    }

    public function setKolom()
    {
        if (!empty($this->session->userdata('idowner'))) {
            $key = getCourse()->row_array()['idtempatkursus'];
            $this->cond = ['idtempatkursus' => $key];
            $a_tempat = $this->tempatkursus->getListCombo(['idtempatkursus' => $key]);
            $a_paket = $this->paketkursus->getListCombo(['idtempatkursus' => $key]);
            $a_peserta = $this->peserta->getListCombo();
        } else {
            $a_tempat = $this->tempatkursus->getListCombo();
            $a_paket = $this->paketkursus->getListCombo();
            $a_peserta = $this->peserta->getListCombo();
        }

        $a_kolom = [];
        $a_kolom[] = ['kolom' => ':no', 'label' => 'No', 'is_null' => true];
        $a_kolom[] = ['kolom' => 'noregistrasi', 'label' => 'No Registrasi'];
        $a_kolom[] = ['kolom' => 'idpeserta', 'label' => 'Nama', 'type' => 'S', 'option' => $a_peserta];
        $a_kolom[] = ['kolom' => 'idpaketkursus', 'label' => 'Paket Kursus', 'type' => 'S', 'option' => $a_paket];
        $a_kolom[] = ['kolom' => 'idtempatkursus', 'label' => 'Tempat Kursus', 'type' => 'S', 'option' => $a_tempat];
        $a_kolom[] = ['kolom' => 'waktu', 'label' => 'Waktu'];

        $this->a_kolom = $a_kolom;
    }
}
