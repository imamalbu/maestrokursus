<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Request extends MY_Controller
{
    public $user;

    public function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('username')) {
            redirect('auth');
        }

        $query = "SELECT seenby_admin FROM tempatkursus WHERE seenby_admin=0";
        $count = $this->db->query($query)->num_rows();

        if($count > 0){
            $this->db->where('seenby_admin', 0);
            $this->db->update('tempatkursus', ['seenby_admin' => 1]);
        }

        $this->load->model('M_tempatkursus', 'tempatkursus');
        $this->load->model('M_pemilik', 'pemilik');
        $this->load->model('M_user', 'users');
        $this->load->library('PDF');
        $this->load->helper('download');
    }

    public function index()
    {
        $this->breadcrumb->append_crumb('<i class="fa fa-home"></i> Beranda', site_url());
        $this->breadcrumb->append_crumb('Request', site_url('request'));

        $data['title'] = 'Request Partner';
        $data['profile'] = 'Request';
        $data['user'] = $this->user;
        $data['request'] = $this->tempatkursus->getWithOwner()->result_array();

        $this->template->load('template', 'request/index', $data);
    }

    public function detail($id)
    {
        $data['title'] = 'Detail Partner';
        $data['user'] = $this->user;
        $data['detail'] = $this->pemilik->getAllData($id)->row_array();
        $this->template->load('template', 'request/detail', $data);
    }

    public function setData($id, $status)
    {
        $data['detail'] = $this->pemilik->getAllData($id)->row_array();
        $username = explode('@', $data['detail']['emailcenter'])[0];
        $pass = password_hash('xyz' . $username . '321', PASSWORD_DEFAULT);

        $user = [
            'name' => $data['detail']['namapemilik'],
            'username' => $username,
            'email' => $data['detail']['emailcenter'],
            'idowner' => $data['detail']['idowner'],
            'image' => 'default.jpg',
            'password' => $pass,
            'role_id' => 2,
            'is_active' => 1,
            'date_created' => time()
        ];

        if ($status > 1) {
            $this->tempatkursus->update(['is_accepted' => $status], $id);
            $this->users->insert($user);
            $ok = $this->tempatkursus->statusTrans();
            $this->tempatkursus->commitTrans($ok);

            $ok && $ok ? setMessage('Berhasil merubah status', 'success') : setMessage('Gagal merubah status', 'danger');
        } else {
            $ok = $this->tempatkursus->update(['is_accepted' => $status], $id);
            $ok ? setMessage('Berhasil merubah status', 'success') : setMessage('Gagal merubah status', 'danger');
        }

        redirect('request');
    }

    public function printPDF($id, $status)
    {
        $PDF = new \Mpdf\Mpdf();
        $data['detail'] = $this->pemilik->getAllData($id)->row_array();
        $data['username'] = explode('@', $data['detail']['emailcenter'])[0];
        $data['password'] = 'xyz' . $data['username'] . '321';
        $data['status'] = $status;
        $data['msg'] = $status > 1 ? 'lembaga kursus anda diterima menjadi partner PT.Maestro Indonesia' : 'lembaga kursus anda tidak memenuhi persyaratan kami';
        $view = $this->load->view('laporan/index', $data, TRUE);
        $PDF->WriteHTML($view);
        $PDF->Output('suratkeputusan.pdf', 'D');
    }
}
