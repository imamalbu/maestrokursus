<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User extends CI_Controller
{
    public $user;

    public function __construct()
    {
        parent::__construct();
        // is_logged_in();
        if (!$this->session->userdata('username')) {
            redirect('auth');
        }

        $this->load->model('M_user', 'users');
        $this->user = $this->users->getBy(['username' => $this->session->userdata('username')])->row_array();
    }

    public function index()
    {
        $this->breadcrumb->append_crumb('<i class="fa fa-home"></i> Beranda', site_url());
        $this->breadcrumb->append_crumb('Profil', '#');
        $this->breadcrumb->append_crumb('Detail', '#');

        $data['title'] = 'My Profile';
        $data['profile'] = 'My Profile';
        $data['user'] = $this->user;

        $this->template->load('template', 'user/index', $data);
    }

    public function edit()
    {
        $this->breadcrumb->append_crumb('<i class="fa fa-home"></i> Beranda', site_url());
        $this->breadcrumb->append_crumb('Profil', '#');
        $this->breadcrumb->append_crumb('Ubah profil', '#');

        $data['title'] = 'Edit Profile';
        $data['profile'] = 'Edit Profile';
        $data['user'] = $this->user;

        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required');

        if ($this->form_validation->run() == false) {
            $this->template->load('template', 'user/edit', $data);
        } else {
            $key = $this->input->post('key');
            $name = $this->input->post('name');
            $username = $this->input->post('username');
            $email = $this->input->post('email');

            //cek jika ada inputan gambar
            $image = $_FILES['image']['name'];

            if ($image) {
                $image = $username . "." . pathinfo($image, PATHINFO_EXTENSION);

                $config['allowed_types'] = 'jpg|png';
                $config['max_size'] = '1048';
                $config['upload_path'] = './assets/img/profile/';
                $config['file_name'] = $image;

                $this->load->library('upload', $config);

                if ($this->upload->do_upload('image')) {
                    $old_image = $data['user']['image'];

                    if ($old_image != 'default.jpg') {
                        unlink(FCPATH . 'assets/img/profile/' . $old_image);
                    }

                    $new_image = $this->upload->data('file_name');
                    $this->db->set('image', $new_image);
                } else {
                    echo $this->upload->display_errors();
                }
            }

            $data = [
                'username' => $username,
                'name' => $name,
                'email' => $email,
                'image' => $image
            ];

            $rowUsername = $this->users->getBy(['username' => $username])->num_rows();
            $rowEmail = $this->users->getBy(['email' => $email])->num_rows();
            $getUsername = $this->users->getBy(['username' => $username])->row_array();
            $getEmail = $this->users->getBy(['email' => $email])->row_array();

            if ($rowUsername < 1 || $rowEmail < 1) {
                $update = $this->users->update($data, $key);
                $msg = 'merubah profil';
                $update ? setMessage('Berhasil ' . $msg, 'success') : setMessage('Gagal ' . $msg, 'danger');
                $this->session->unset_userdata(['username', 'email']);
                $this->session->set_userdata(['username' => $username, 'email' => $email]);
                redirect('user');
            } else {
                if ($getUsername['id'] == $key && $getEmail['id'] == $key) {
                    $update = $this->users->update($data, $key);
                    $msg = 'merubah profil';
                    $update ? setMessage('Berhasil ' . $msg, 'success') : setMessage('Gagal ' . $msg, 'danger');
                    redirect('user');
                } else {
                    if ($getUsername['id'] != $key && $getEmail['id'] != $key) {
                        setMessage('Username dan Email telah digunakan', 'danger');
                    } else if ($getUsername['id'] != $key) {
                        setMessage('Username telah digunakan', 'danger');
                    } else if ($getEmail['id'] != $key) {
                        setMessage('Email telah digunakan', 'danger');
                    }

                    redirect('user/edit');
                }
            }
        }
    }

    public function changePassword()
    {
        $this->breadcrumb->append_crumb('<i class="fa fa-home"></i> Beranda', site_url());
        $this->breadcrumb->append_crumb('Profil', '#');
        $this->breadcrumb->append_crumb('Ubah passwrod', '#');

        $data['title'] = 'Change Password';
        $data['profile'] = 'Change Password';
        $data['user'] = $this->user;

        $this->form_validation->set_rules('current_password', 'Current password', 'required|trim');
        $this->form_validation->set_rules('new_password1', 'New password', 'required|trim|min_length[8]|matches[new_password2]');
        $this->form_validation->set_rules('new_password2', 'Confirm new password', 'required|trim|min_length[8]|matches[new_password1]');

        if ($this->form_validation->run() == false) {
            $this->template->load('template', 'user/changepassword', $data);
        } else {
            $current_password = $this->input->post('current_password');
            $new_password1 = $this->input->post('new_password1');
            $new_password2 = $this->input->post('new_password2');

            if (!password_verify($current_password, $data['user']['password'])) {
                $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
            Wrong password!</div>');
                redirect('user/changepassword');
            } else {
                if ($current_password == $new_password1) {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
            New password cannot be the same as current password!
            </div>');
                    redirect('user/changepassword');
                } else {
                    //password sudah ok , acak password
                    $password_hash = password_hash($new_password1, PASSWORD_DEFAULT);

                    $this->db->set('password', $password_hash);
                    $this->db->where('email', $this->session->userdata('email'));
                    $this->db->update('users');

                    $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
                    Password changed!
                    </div>');
                    redirect('user/changepassword');
                }
            }
        }
    }

    public function mydata()
    {
    }
}
