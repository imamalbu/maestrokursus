<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_jeniskursus extends MY_Model
{
    protected $table = 'jeniskursus';
    protected $schema = '';
    public $key = 'idjeniskursus';
    public $value = 'jeniskursus';

    function __construct()
    {
        parent::__construct();
    }
}
