<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_konten extends MY_Model
{
    protected $table = 'konten';
    protected $schema = '';
    public $key = 'idkonten';
    public $value = 'judul';

    function __construct()
    {
        parent::__construct();
    }

    public function getReference($id = null)
    {
        $where = empty($id) ? '' : " WHERE idkonten=$id";
        $query = "SELECT * FROM konten JOIN jeniskonten USING(idjeniskonten)" . $where;

        return $this->db->query($query);
    }

    public function getReferences($id = null)
    {
        $where = empty($id) ? '' : " WHERE idjeniskonten=$id";
        $query = "SELECT * FROM konten JOIN jeniskonten USING(idjeniskonten)" . $where;

        return $this->db->query($query);
    }
}
