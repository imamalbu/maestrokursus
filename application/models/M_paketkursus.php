<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_paketkursus extends MY_Model
{
    protected $table = 'paketkursus';
    protected $schema = '';
    public $key = 'idpaketkursus';
    public $value = 'namapaketkursus';

    function __construct()
    {
        parent::__construct();
    }

    public function getPaket($id = null, $index = null)
    {
        if (empty($index)) {
            $where = !empty($id) ? " WHERE pk.idpaketkursus=$id " : "";
        } else {
            $where = !empty($id) ? " WHERE pk.$index=$id " : "";
        }


        $query = "SELECT jk.jeniskursus,pk.idpaketkursus,pk.namapaketkursus,pk.jadwal,pk.silabus,tk.namakursus,tk.idtempatkursus,tk.kota,tk.provinsi,tk.alamatkursus,tk.callcenter,tk.emailcenter,pk.durasi,pk.harga,pk.kuota,pk.deskripsi,pk.foto,ins.namainstruktur FROM paketkursus pk JOIN tempatkursus tk USING(idtempatkursus) JOIN jeniskursus jk ON pk.idjeniskursus=jk.idjeniskursus JOIN instruktur_kursus ik USING(idpaketkursus) JOIN instruktur ins ON ik.idinstruktur=ins.idinstruktur" . $where ;
        return $this->db->query($query);
    }

    public function getPaketBeranda($id = null, $index = null)
    {
        if (empty($index)) {
            $where = !empty($id) ? " WHERE pk.idpaketkursus=$id " : " ";
        } else {
            $where = !empty($id) ? " WHERE pk.$index=$id " : " ";
        }


        $query = "SELECT jk.jeniskursus,pk.idpaketkursus,pk.namapaketkursus,pk.jadwal,pk.silabus,tk.namakursus,tk.idtempatkursus,tk.kota,tk.provinsi,tk.alamatkursus,tk.callcenter,tk.emailcenter,pk.durasi,pk.harga,pk.kuota,pk.deskripsi,pk.foto,ins.namainstruktur FROM paketkursus pk JOIN tempatkursus tk USING(idtempatkursus) JOIN jeniskursus jk ON pk.idjeniskursus=jk.idjeniskursus JOIN instruktur_kursus ik USING(idpaketkursus) JOIN instruktur ins ON ik.idinstruktur=ins.idinstruktur" . $where . 'LIMIT 3' ;
        return $this->db->query($query);
    }
}
