<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_pembayaran extends MY_Model
{
    protected $table = 'pembayaran';
    protected $schema = '';
    public $key = 'idpembayaran';
    public $value = 'invoice';

    function __construct()
    {
        parent::__construct();
    }

    public function getPembayaran($id = null)
    {
        $where = empty($id) ? '' : " WHERE dp.idtempatkursus=$id";
        $q = "SELECT p.idpembayaran,p.total,p.status,p.buktibayar,p.invoice,dp.waktu,dp.idtempatkursus,p.iddaftar,ps.namapeserta,pk.namapaketkursus FROM pembayaran p JOIN daftar_peserta dp ON p.iddaftar=dp.iddaftar JOIN paketkursus pk ON dp.idpaketkursus=pk.idpaketkursus JOIN peserta ps ON dp.idpeserta=ps.idpeserta" . $where;
        return $this->db->query($q);
    }
}
