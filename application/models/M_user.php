<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_user extends MY_Model
{
    protected $table = 'users';
    protected $schema = '';
    public $key = 'id';
    public $value = 'name';

    function __construct()
    {
        parent::__construct();
    }

    public function getUsers($limit, $start, $keyword = null)
    {
        if ($keyword) {
            $this->db->like('username', $keyword);
        }
        return $this->db->get('users', $limit, $start)->result_array();
    }

    public function auth($username)
    {
        $query = "SELECT u.id,u.username,u.email,u.role_id,u.password,u.is_active,p.idpeserta,pm.idowner FROM users u LEFT JOIN peserta p ON u.idpeserta=p.idpeserta LEFT JOIN pemilik pm ON u.idowner=pm.idowner WHERE username='$username'";
        return $this->db->query($query);
    }
}
