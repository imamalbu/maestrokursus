    <!-- ##### Hero Area Start ##### -->
    <section class="hero-area bg-img bg-overlay-2by5" style="background-image: url(<?= base_url('assets/img/konten/' . settingSIM()['img_jumbotron']) ?>);">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12">
                    <!-- Hero Content -->
                    <div class="hero-content text-center">
                        <h2 style="text-shadow: 2px 2px 4px #000000;"><?= settingSIM()['title_jumbotron']; ?></h2>
                        <a href="<?= base_url('partner') ?>" class="btn btn-warning text-dark">Daftar Jadi Partner</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ##### Hero Area End ##### -->

    <!-- ##### Cool Facts Area Start ##### -->
    <section class="cool-facts-area section-padding-100-0">
        <div class="container">
            <div class="row d-flex justify-content-center">
                <!-- Single Cool Facts Area -->
                <div class="col-12 col-sm-6 col-lg-3">
                    <div class="single-cool-facts-area text-center mb-100 wow fadeInUp" data-wow-delay="250ms">
                        <div class="icon">
                            <img src="<?= base_url('assets/img/core-img/earth.png') ?>" alt="">
                        </div>
                        <h2><span class="counter"><?= $a_data['daftarpeserta'] ?></span></h2>
                        <h5>Jumlah peserta</h5>
                    </div>
                </div>

                <!-- Single Cool Facts Area -->
                <div class="col-12 col-sm-6 col-lg-3">
                    <div class="single-cool-facts-area text-center mb-100 wow fadeInUp" data-wow-delay="750ms">
                        <div class="icon">
                            <img src="<?= base_url('assets/img/core-img/docs.png') ?>" alt="">
                        </div>
                        <h2><span class="counter"><?= $a_data['paketkursus'] ?></span></h2>
                        <h5>Kursus</h5>
                    </div>
                </div>

                <!-- Single Cool Facts Area -->
                <div class="col-12 col-sm-6 col-lg-3">
                    <div class="single-cool-facts-area text-center mb-100 wow fadeInUp" data-wow-delay="500ms">
                        <div class="icon">
                            <img src="<?= base_url('assets/img/core-img/star.png') ?>" alt="">
                        </div>
                        <h2><span class="counter"><?= $a_data['instruktur'] ?></span></h2>
                        <h5>Instruktur</h5>
                    </div>
                </div>



            </div>
        </div>
    </section>
    <!-- ##### Cool Facts Area End ##### -->

    <!-- ##### Popular Courses Start ##### -->
    <section class="popular-courses-area section-padding-100-0" style="background-image: url(img/core-img/texture.png);">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section-heading">
                        <h3>Daftar Kursus Terfavorit</h3>
                    </div>
                </div>
            </div>

            <div class="row">
                <!-- Single Popular Course -->
                <?php foreach ($paketkursus as $pk) : ?>
                    <div class="col-12 col-md-6 col-lg-4">
                        <div class="single-popular-course mb-100 wow fadeInUp" data-wow-delay="250ms">
                            <img src="<?= base_url('assets/doc/datapaket/' . $pk['foto']) ?>" alt="">
                            <!-- Course Content -->
                            <div class="course-content">
                                <h4></h4>
                                <div class="meta d-flex align-items-center">
                                    <table>
                                        <tr>
                                            <td><a href="#"><?= $pk['namainstruktur'] ?></a></td>
                                        </tr>
                                    </table>
                                </div>
                                <table>
                                    <tr>
                                        <td>
                                            <a href="<?= base_url('kursus/detail/' . $pk['idpaketkursus']) ?>">
                                                <h5><?= $pk['namapaketkursus'] ?></h5>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <h5 class="text-primary"><?= toRupiah($pk['harga']) ?></h5>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><b class="text-secondary"><?= $pk['kota'] ?></b></td>
                                    </tr>
                                </table>
                                <p align="justify"><?= substr($pk['deskripsi'], 0, 142) . ' ...'; ?></p>
                            </div>
                            <!-- Seat Rating Fee -->
                            <div class="seat-rating-fee d-flex justify-content-between">
                                <div class="seat-rating h-100 d-flex align-items-center">
                                    <div class="seat">
                                        <i class="fa fa-user" aria-hidden="true"></i> 10
                                    </div>
                                    <div class="rating">
                                        <i class="fa fa-star" aria-hidden="true"></i> 4.5
                                    </div>
                                    <div class="course-fee h-100">
                                        <p class="mt-2 text-primary"><b><?= $pk['jeniskursus'] ?></b></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </section>

    <div class="text-center">
        <a href="<?= base_url('kursus') ?>">
            <h5 class="text-primary">Lihat Selengkapnya</h5>
        </a>
    </div>
    <!-- ##### Popular Courses End ##### -->

    <!-- ##### Best Tutors Start ##### -->
    <section class="best-tutors-area section-padding-100">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section-heading">
                        <h3>Para Instruktur Terbaik</h3>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12">
                    <div class="tutors-slide owl-carousel wow fadeInUp" data-wow-delay="250ms">
                        <?php foreach ($instruktur as $ins) : ?>
                            <!-- Single Tutors Slide -->
                            <div class="single-tutors-slides">
                                <!-- Tutor Thumbnail -->
                                <div class="tutor-thumbnail">
                                    <img src="<?= base_url('assets/img/instruktur/') . $ins['foto'] ?>" alt="">
                                </div>
                                <!-- Tutor Information -->
                                <div class="tutor-information text-center">
                                    <h5><?= $ins['namainstruktur'] ?></h5>
                                    <span><?= $ins['lulusan'] ?></span>
                                    <p>From <b><?= $ins['namakursus']; ?></b></p>
                                    <div class="social-info">
                                        <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                        <a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                                        <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ##### Best Tutors End ##### -->

    <!-- ##### Upcoming Events Start ##### -->
    <section class="upcoming-events section-padding-100-0">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section-heading">
                        <h3>Event Terdekat</h3>
                    </div>
                </div>
            </div>

            <div class="row">
                <!-- Single Blog Area -->
                <?php foreach ($a_event as $event) : ?>
                    <div class="col-12 col-md-4">
                        <div class="single-blog-area mb-100 wow fadeInUp" data-wow-delay="250ms">
                            <img src="<?= base_url('assets/img/konten/' . $event['gambar']) ?>" alt="">
                            <!-- Blog Content -->
                            <div class="blog-content">
                                <a href="<?= base_url('info/detail/' . $event['idkonten']) ?>" class="blog-headline">
                                    <h4><?= $event['judul'] ?></h4>
                                </a>
                                <div class="meta d-flex align-items-center">
                                    <a href="#"><?= $event['subjudul'] ?></a>
                                </div>
                                <p>Upload at : <?= $event['time'] ?></p>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </section>
    <!-- ##### Upcoming Events End ##### -->

    <!-- ##### Blog Area Start ##### -->
    <section class="blog-area section-padding-100-0">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section-heading">
                        <h3>Lowongan Pekerjaan</h3>
                    </div>
                </div>
            </div>

            <div class="row">
                <!-- Single Blog Area -->
                <?php foreach ($a_lowongan as $lowongan) : ?>
                    <div class="col-12 col-md-4">
                        <div class="single-blog-area mb-100 wow fadeInUp" data-wow-delay="250ms">
                            <img src="<?= base_url('assets/img/konten/' . $lowongan['gambar']) ?>" alt="">
                            <!-- Blog Content -->
                            <div class="blog-content">
                                <a href="<?= base_url('info/detail/' . $lowongan['idkonten']) ?>" class="blog-headline">
                                    <h4><?= $lowongan['judul'] ?></h4>
                                </a>
                                <div class="meta d-flex align-items-center">
                                    <a href="#"><?= $lowongan['subjudul'] ?></a>
                                </div>
                                <p>Upload at : <?= $lowongan['time'] ?></p>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </section>
    <!-- ##### Blog Area End ##### -->