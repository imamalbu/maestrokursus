<!-- ##### Footer Area Start ##### -->
<footer class="footer-area">
    <!-- Top Footer Area -->
    <div class="top-footer-area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <!-- Footer Logo -->
                    <div class="footer-logo">
                        <a href="<?= base_url('beranda') ?>">
                            <h4 class="text-white"><?= settingSIM()['app_name'] ?></h4>
                        </a>
                    </div>
                    <!-- Copywrite -->
                    <p><a href="#">
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                            Copyright &copy;<script>
                                document.write(new Date().getFullYear());
                            </script> All rights reserved by <a href="<?= base_url('beranda') ?>">Maestro Kursus</a>
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    </p>
                </div>
            </div>
        </div>
    </div>

    <!-- Bottom Footer Area -->
    <div class="bottom-footer-area d-flex justify-content-between align-items-center">
        <!-- Contact Info -->
        <div class="contact-info">
            <a href="#"><span>Phone:</span> <?= settingSIM()['call_center'] ?></a>
            <a href="#"><span>Email:</span> <?= settingSIM()['email_center'] ?></a>
        </div>
        <!-- Follow Us -->
        <div class="follow-us">
            <span>Follow us</span>
            <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
            <a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a>
            <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
        </div>
    </div>
</footer>
<!-- ##### Footer Area End ##### -->

<!-- ##### All Javascript Script ##### -->
<!-- jQuery-2.2.4 js -->
<!-- Bootstrap core JavaScript-->
<script src="<?= base_url('assets/client/js/jquery/jquery-2.2.4.min.js') ?>"></script>
<!-- Popper js -->
<script src="<?= base_url('assets/client/js/bootstrap/popper.min.js') ?>"></script>
<!-- Bootstrap js -->
<script src="<?= base_url('assets/client/js/bootstrap/bootstrap.min.js') ?>"></script>
<!-- All Plugins js -->
<script src="<?= base_url('assets/client/js/plugins/plugins.js') ?>"></script>
<!-- Active js -->
<script src="<?= base_url('assets/client/js/active.js') ?>"></script>
</body>

</html>