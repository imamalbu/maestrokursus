<!-- ##### Breadcumb Area Start ##### -->
<div class="breadcumb-area">
    <!-- Breadcumb -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item"><a href="#">Courses</a></li>
            <li class="breadcrumb-item"><a href="#">Art &amp; Design</a></li>
            <li class="breadcrumb-item active" aria-current="page">English Grammer</li>
        </ol>
    </nav>
</div>
<!-- ##### Breadcumb Area End ##### -->

<!-- ##### Single Course Intro Start ##### -->
<div class="single-course-intro d-flex align-items-center justify-content-center" style="background-image: url(<?= base_url('assets/doc/datapaket/' . $detail['foto']) ?>);">
    <!-- Content -->
    <div class="single-course-intro-content text-center">
        <!-- Ratings -->
        <div class="ratings">
            <i class="fa fa-star" aria-hidden="true"></i>
            <i class="fa fa-star" aria-hidden="true"></i>
            <i class="fa fa-star" aria-hidden="true"></i>
            <i class="fa fa-star" aria-hidden="true"></i>
            <i class="fa fa-star-o" aria-hidden="true"></i>
        </div>
        <h3><?= $detail['namapaketkursus'] ?></h3>
        <div class="meta d-flex align-items-center justify-content-center">
            <a href="#"><?= $detail['namakursus'] ?></a>
            <span><i class="fa fa-circle" aria-hidden="true"></i></span>
            <a href="#"><?= $detail['jeniskursus'] ?></a>
        </div>
        <div class="price">
            <h5 class="text-primary"><?= toRupiah($detail['harga']) ?></h5>
        </div>
    </div>
</div>
<!-- ##### Single Course Intro End ##### -->

<!-- ##### Courses Content Start ##### -->
<div class="single-course-content section-padding-100">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-8">
                <div class="course--content">

                    <div class="clever-tabs-content">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="tab--1" data-toggle="tab" href="#tab1" role="tab" aria-controls="tab1" aria-selected="false">Deskripsi</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="tab--2" data-toggle="tab" href="#tab2" role="tab" aria-controls="tab2" aria-selected="true">Kurikulum</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="tab--3" data-toggle="tab" href="#tab3" role="tab" aria-controls="tab3" aria-selected="true">Jadwal</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="tab--5" data-toggle="tab" href="#tab5" role="tab" aria-controls="tab5" aria-selected="true">Forum</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="tab--5" data-toggle="tab" href="#tab6" role="tab" aria-controls="tab5" aria-selected="true">Kontak</a>
                            </li>
                        </ul>
                        <?= $this->session->flashdata('message') ?>
                        <div class="tab-content" id="myTabContent">
                            <!-- Tab Text -->
                            <div class="tab-pane fade show active" id="tab1" role="tabpanel" aria-labelledby="tab--1">
                                <div class="clever-description">

                                    <!-- About Course -->
                                    <div class="about-course mb-30">
                                        <h4>Tentang paket kursus</h4>
                                        <p align="justify"><?= $detail['deskripsi'] ?></p>
                                    </div>

                                    <!-- All Instructors -->
                                    <div class="all-instructors mb-30">
                                        <h4>Instruktur</h4>

                                        <div class="row">
                                            <!-- Single Instructor -->
                                            <?php foreach ($instruktur as $ins) : ?>
                                                <div class="col-lg-6">
                                                    <div class="single-instructor d-flex align-items-center mb-30">
                                                        <div class="instructor-thumb">
                                                            <img src="<?= base_url('assets/img/instruktur/' . $ins['foto']) ?>" alt="">
                                                        </div>
                                                        <div class="instructor-info">
                                                            <h6><?= $ins['namainstruktur'] ?></h6>
                                                            <span><?= $ins['lulusan']; ?></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Tab Text -->
                            <div class="tab-pane fade" id="tab2" role="tabpanel" aria-labelledby="tab--2">
                                <div class="clever-curriculum">

                                    <!-- About Curriculum -->
                                    <div class="about-curriculum mb-30">
                                        <h4>Silabus</h4>
                                        <p align="justify">
                                            Silabus adalah salah satu komponen perangkat pembelajaran dari rencana pembelajaran pada suatu kelompok mata pelajaran dengan tema tertentu, yang mencakup standar kompetensi, kompetensi dasar, materi pembelajaran, indikator, penilaian, alokasi waktu, dan sumber belajar yang dikembangkan oleh setiap satuan pendidikan.
                                        </p>
                                        <a href="<?= base_url('assets/doc/datapaket/' . $detail['silabus']) ?>" class="btn btn-primary" target="_blank">Download Silabus <i class="fa fa-download ml-1"></i></a>
                                    </div>
                                </div>
                            </div>
                            <!-- Tab Text -->
                            <div class="tab-pane fade" id="tab3" role="tabpanel" aria-labelledby="tab--3">
                                <div class="clever-curriculum">

                                    <!-- About Curriculum -->
                                    <div class="about-curriculum mb-30">
                                        <h4>Jadwal</h4>
                                        <p align="justify">
                                            Sebelum anda mengikuti kursus <?= $detail['namapaketkursus'] ?> disarankan untuk melihat jadwal yang telah kami sediakan terlebih dahulu.
                                        </p>
                                        <a href="<?= base_url('assets/doc/datapaket/' . $detail['jadwal']) ?>" class="btn btn-primary" target="_blank">Download Jadwal <i class="fa fa-download ml-1"></i></a>
                                    </div>
                                </div>
                            </div>

                            <!-- Tab Text -->
                            <div class="tab-pane fade" id="tab5" role="tabpanel" aria-labelledby="tab--5">
                                <div class="clever-review">

                                    <!-- About Review -->
                                    <div class="about-review mb-30">
                                        <h4>Reviews</h4>
                                        <p align="justify">Anda dapat melihat review dari peserta yang telah mengikuti kursus ini sebagai referensi anda agar lebih yakin untuk mengikuti kursus <?= $detail['namapaketkursus'] ?></p>
                                    </div>

                                    <!-- Single Review -->
                                    <div class="single-review mb-30">
                                        <div class="d-flex justify-content-between mb-15">
                                            <!-- Review Admin -->
                                            <div class="review-admin d-flex">
                                                <div class="text">
                                                    <h3>Komentar</h3>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="container">
                                            <form action="<?= base_url('kursus/komentar') ?>" method="POST">
                                                <div class="form-group">
                                                    <textarea class="form-control" name="komentar" id="komentar" rows="4" placeholder="Tuliskan komentar anda ..." required></textarea>
                                                </div>
                                                <input type="hidden" name="paketkursus" value="<?= $detail['idpaketkursus'] ?>">
                                                <input type="hidden" name="peserta" value="<?= $this->session->userdata('idpeserta'); ?>">
                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-primary" name="btnsend">Kirim <i class="fa fa-send"></i></button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>


                                    <!-- Single Review -->
                                    <?php foreach ($komentar as $k) : ?>
                                        <div class="single-review mb-30">
                                            <div class="d-flex justify-content-between mb-30">
                                                <!-- Review Admin -->
                                                <div class="review-admin d-flex">
                                                    <div class="thumb">
                                                        <img src="<?= base_url('assets/img/user.png') ?>" alt="">
                                                    </div>
                                                    <div class="text">
                                                        <h6><?= $k['namapeserta'] ?></h6>
                                                        <span><?= $k['waktu'] ?></span>
                                                    </div>
                                                </div>
                                                <!-- Ratings -->
                                                <?php if ($this->session->userdata('idpeserta') == $k['idpeserta']) : ?>
                                                    <a href="<?= base_url('kursus/hapuskomentar/' . $k['idkomentar']) . '/' . $detail['idpaketkursus'] ?>" class="text-danger">Hapus</a>
                                                <?php endif; ?>
                                            </div>
                                            <p><?= $k['komentar'] ?></p>
                                        </div>
                                    <?php endforeach ?>
                                </div>
                            </div>

                                                        <!-- Tab Text -->
                            <div class="tab-pane fade" id="tab6" role="tabpanel" aria-labelledby="tab--6">
                                <div class="clever-curriculum">

                                    <!-- About Curriculum -->
                                    <div class="about-curriculum mb-30">
                                        <h4>Kontak</h4>
                                        <p>
                                            Alamat Kursus : <?=$detail['alamatkursus'] ?> <br>
                                            No telp : <?= $detail['callcenter'] ?> <br>
                                            Email : <?= $detail['emailcenter'] ?> <br>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-12 col-lg-4">
                <div class="course-sidebar">
                    <!-- Buy Course -->
                    <form action="<?= base_url('kursus/daftar') ?>" method="POST" id="formdaftar">
                        <input type="hidden" name="tempatkursus" value="<?= $detail['idtempatkursus'] ?>">
                        <input type="hidden" name="paketkursus" value="<?= $detail['idpaketkursus'] ?>">
                        <input type="hidden" name="harga" value="<?= $detail['harga'] ?>">
                        <input type="hidden" name="peserta" value="<?= $this->session->userdata('idpeserta') ?>">
                        <button type="button" data-type="btn-daftar" class="btn clever-btn mb-30 w-100">Ikut Kursus</button>
                    </form>

                    <!-- Widget -->
                    <div class="sidebar-widget">
                        <h4>Fitur Kursus</h4>
                        <ul class="features-list">
                            <li>
                                <h6><i class="fa fa-clock-o" aria-hidden="true"></i> Durasi</h6>
                                <h6><?= $detail['durasi'] ?></h6>
                            </li>
                            <li>
                                <h6><i class="fa fa-users" aria-hidden="true"></i> Kuota</h6>
                                <h6><?= $detail['kuota'] ?></h6>
                            </li>
                            <li>
                                <h6><i class="fa fa-database" aria-hidden="true"></i> Harga</h6>
                                <h6 class="text-primary"><?= toRupiah($detail['harga']) ?></h6>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>


<div class="modal fade" id="daftarModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5>Daftar Kursus</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">Apakah anda yakin akan mendaftar kursus <?= $detail['namapaketkursus']; ?> ? </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                <button type="button" data-type="btn-confirm" class="btn btn-primary" href="<?= base_url('auth/logout'); ?>">Iya</button>
            </div>
        </div>
    </div>
</div>
<!-- ##### Courses Content End ##### -->
<script>
    $('[data-type=btn-daftar]').click(function() {
        var username = '<?= $this->session->userdata('username') ?>';
        var modal = $('#daftarModal');
        var data = '<?= checkData(); ?>';

        if (username == "") {
            location.href = '<?= base_url('auth') ?>';
        } else if (data == "") {
            location.href = '<?= base_url('profil/data') ?>';
        } else if (username != "") {
            modal.modal();
        }
    });

    $('[data-type=btn-confirm]').click(function() {
        var form = $('#formdaftar');
        form.submit();
    });
</script>