<div class="container mt-5 mb-5">
    <div class="row d-flex justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <?php if ($detail['status'] < 1) :  ?>
                        <div class="row justify-content-center mb-5 pb-3">
                            <div class="col-md-7 heading-section ftco-animate text-center">
                                <h3 class="mt-3"><?= settingSIM()['company_name'] ?></h3>
                                <span class=""><b>Pendaftaran Berhasil</b></span>
                                <h5 class="mb-2"><?= $detail['invoice'] ?></h5>
                            </div>
                            <div class="col-md-7 heading-section ftco-animate">
                                <p align="center">Jumlah yang harus di bayar adalah <strong><?= toRupiah($detail['total']) ?></strong></p>
                                <h4 align="center" id="waktubayar" class="mb-3">Expired Time</h4>
                                <p>Pembayaran harap di transfer melelui rekening di bawah ini</p>
                                <?= settingSIM()['rek_bank'] ?>
                                <br>
                                <p>Setelah melakukan pembayaran, harap melakukan konfirmasi dan upload bukti pembayaran melalui menu Kursus Saya -> Detail -> Upload Pembayaran.</p>
                                <p><b>Terimakasih.</b></p>
                            </div>
                        </div>
                    <?php else : ?>
                        <div class="row justify-content-center mb-5 pb-3">
                            <h3 class="mt-3">Berhasil Melakukan Pembayaran</h3>
                            <p>Selamat anda telah terdaftar sebagai peserta pelatihan kursus.</p>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var expired = '<?= $detail['expired_at'] ?>';
    var countDownDate = new Date(expired).getTime();

    // Update the count down every 1 second
    var x = setInterval(function() {

        // Get today's date and time
        var now = new Date().getTime();

        // Find the distance between now and the count down date
        var distance = countDownDate - now;

        // Time calculations for days, hours, minutes and seconds
        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);

        // Display the result in the element with id="demo"
        document.getElementById("waktubayar").innerHTML = days + "d " + hours + "h " +
            minutes + "m " + seconds + "s ";

        // If the count down is finished, write some text
        if (distance < 0) {
            clearInterval(x);
            document.getElementById("waktubayar").innerHTML = "EXPIRED";
            location.href = '<?= base_url('kursus/deletepeserta/' . $detail['iddaftar']) ?>';
        }
    }, 1000);
</script>