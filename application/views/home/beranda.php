<!-- Main Content -->
<div id="content">
    <!-- Begin Page Content -->
    <div class="container-fluid">

        <div class="row">
            <div class="col-lg-6">
                <?= $this->session->flashdata('message'); ?>
            </div>
        </div>
        <div class="jumbotron">
            <h1 class="display-4">Selamat datang, <?= $user['name']; ?></h1>
            <p class="lead">
                Sistem Informasi <?php if (!empty($this->session->userdata('idowner'))) : ?>
                    <?= getCourse()->row_array()['namakursus']; ?>
                <?php else : ?>
                    <?= settingSIM()['app_name'] ?>
                <?php endif ?>
            </p>
        </div>
        <div class="row">
            <!-- Earnings (Monthly) Card Example -->
            <?php foreach ($info as $val) : ?>
                <?php if (!isset($val['role']) || $val['role'] == $this->session->userdata('role_id')) : ?>
                    <div class="col-xl-3 col-md-6 mb-4">
                        <div class="card border-left-<?= !isset($val['color']) ? 'primary' : $val['color'] ?> shadow h-100 py-2" id="siswa">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1"><?= $val['label'] ?></div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $val['data']; ?></div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="<?= $val['icon'] ?> fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endif ?>
            <?php endforeach ?>
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- End of Main Content -->
</div>