<!-- Main Content -->
<div id="content">
    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Page Heading -->
        <h1 class="h3 mb-4 text-gray-800"><?= $profile; ?></h1>


        <div class="card shadow mb-4">
            <div class="card-body">
                <?php if (!in_array($user['role_id'], array(3, 4))) { ?>
                    <button type="button" data-type="tambah" class="btn btn-primary mb-3">Tambah <?= $title; ?></button>
                <?php } ?>
                <?php if (isset($pelanggar)) { ?>

                    <div class="row">
                        <div class="col-md-12">
                            <table width="100%">
                                <tr>
                                    <td width="150px">NIS</td>
                                    <td width="10px">:</td>
                                    <td><?= $a_info['nis'] ?></td>
                                </tr>
                                <tr>
                                    <td>Nama Siswa</td>
                                    <td>:</td>
                                    <td><?= $a_info['nama'] ?></td>
                                </tr>
                                <tr>
                                    <td>Kelas</td>
                                    <td>:</td>
                                    <td><?= $a_info['namakelasajar'] ?></td>
                                </tr>
                                <tr>
                                    <td>Tahun Ajaran</td>
                                    <td>:</td>
                                    <td><?= $a_info['namatahun'] ?></td>
                                </tr>
                                <tr>
                                    <td>Wali Kelas</td>
                                    <td>:</td>
                                    <td><?= $a_info['namaguru'] ?></td>
                                </tr>
                            </table>
                        </div>
                    </div>

                    <hr>
                    <h5>Detail Pelanggaran</h5>
                    <div class="row">
                        <div class="col-md-12">
                            <?= $this->session->flashdata('message'); ?>
                            <?= $this->session->flashdata('delete'); ?>
                            <form method="post" id="form-list">
                                <table class="table table-responsive table-hover table-striped">
                                    <thead>
                                        <tr>
                                            <th scope="col">No</th>
                                            <th scope="col">Nama Pelanggaran</th>
                                            <th scope="col">Bobot Pelanggaran</th>
                                            <th scope="col">Tgl Pelanggaran</th>
                                            <th scope="col">Skor Didapat</th>
                                            <?php if (!in_array($user['role_id'], array(3, 4))) { ?>
                                                <th scope="col">Aksi</th>
                                            <?php } ?>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            $no = 0;
                                            $skorsiswa = 0;
                                            foreach ($detail as $row) { ?>
                                            <tr>
                                                <td><?= ++$no; ?></td>
                                                <td><?= $row['namapelanggaran'] ?></td>
                                                <td><?= $row['namaskor'] . '(' . $row['skormin'] . ' - ' . $row['skormax'] . ')' ?></td>
                                                <td><?= date('d-m-Y', strtotime($row['tgl_pelanggaran'])); ?></td>
                                                <td><?= $row['skor'] ?></td>
                                                <?php if (!in_array($user['role_id'], array(3, 4))) { ?>
                                                    <td nowrap>
                                                        <button type="button" data-type="edit" class="btn btn-sm btn-info" data-id="<?= $row['idpelanggaran']; ?>">Edit</button>
                                                        <button type="button" data-type="btndelete" class="btn btn-sm btn-danger" data-id="<?= $row['idpelanggaran']; ?>">Delete</a>
                                                    </td>
                                                <?php } ?>
                                            </tr>
                                        <?php
                                                $skorsiswa += (int) $row['skor'];
                                            } ?>
                                        <tr>
                                            <td colspan="4" class="table table-warning" align="right"><strong> Total :</strong></td>
                                            <td colspan="2" class="table table-danger"><?= $skorsiswa ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                                <input type="hidden" name="act" id="act">
                                <input type="hidden" name="key" id="key">
                            </form>
                        </div>
                    </div>

                    <!-- Modal -->
                    <div class="modal fade" id="newMenuModal" tabindex="-1" role="dialog" aria-labelledby="newMenuModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="modal">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="newMenuModalLabel">Pelanggaran Siswa</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <form action="<?= site_url('manajemen/detailPelanggaranSiswa/' . rawurlencode($a_info['nis']) . '/' . $a_info['idtahun']) ?>" method="post" id="modal_post">
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <label for="jenis">Jenis Pelanggaran</label>
                                            <select name="jenis" id="jenis" class="form-control select2" required>
                                                <?php foreach ($jenis as $v) { ?>
                                                    <option value="<?= $v['idjenispelanggaran'] ?>"><?= $v['namapelanggaran'] . ' | Skor : ' . $skor[$v['idskor']] ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="skor">Skor</label>
                                            <input type="number" name="skor" id="skor" class="form-control" required />
                                        </div>
                                        <div class="form-group">
                                            <label for="tgl">Tanggal Pelanggaran</label>
                                            <input type="text" name="tgl" id="tgl" class="form-control datepicker" required />
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="button" data-type="simpan" class="btn btn-success">Simpan</button>
                                    </div>
                                    <input type="hidden" name="act" id="act">
                                    <input type="hidden" name="key" id="key">
                                </form>
                            </div>
                        </div>
                    </div>

                    <div class="modal" tabindex="-1" role="dialog" id="modal-delete">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Hapus Pelanggaran</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <p>Apakah anda ingin menghapus Pelanggaran siswa ini?</p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                                    <button type="button" data-type="delete" class="btn btn-danger">Hapus</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <script>
                        $('[data-type=simpan]').click(function() {
                            var act = $('#modal_post #act').val();
                            var key = $('#modal_post #key').val();
                            if (act == "") {
                                $('#modal_post #act').val('simpan');
                            }
                            $('#modal_post').submit();
                        });


                        $('[data-type=tambah]').click(function() {
                            var modal = $('#newMenuModal');
                            $('#modal_post')[0].reset();
                            modal.find('#newMenuModalLabel').html('Tambah <?= $title ?>');
                            modal.find('#nip').attr('disabled', false);
                            modal.modal();
                        });


                        $('[data-type=btndelete]').click(function() {
                            var id = $(this).attr('data-id');
                            $('#modal-delete').find('[data-type=delete]').attr('data-id', id);
                            $('#modal-delete').modal();
                        });

                        $('[data-type=delete]').click(function() {
                            var id = $(this).attr('data-id');
                            location.href = '<?= site_url('manajemen/deletePelanggaranSiswa/') ?>' + id;
                        });

                        $('[data-type=edit]').click(function() {
                            var id = $(this).attr('data-id');
                            Swal.showLoading();
                            xhrfGetData("<?= site_url('manajemen/getDetailPelanggaran/') ?>" + id, function(data) {
                                var modal = $('#newMenuModal');

                                var date = new Date(data.tgl_pelanggaran);
                                var bulan = ((date.getMonth().length + 1) === 1) ? '0' + (date.getMonth() + 1) : (date.getMonth() + 1);
                                var tgl = date.getDate() + "-" + bulan + "-" + date.getFullYear();

                                modal.find('#newMenuModalLabel').html('Ubah <?= $title ?>');
                                modal.find('#jenis').val(data.idjenispelanggaran);
                                modal.find('#skor').val(data.skor);
                                modal.find('#tgl').val(tgl);
                                modal.find('#act').val('edit');
                                modal.find('#key').val(data.idpelanggaran);
                                Swal.close();
                                modal.modal();
                            });
                        });
                    </script>
                <?php } else { ?>
                    <div class="row">
                        <div class="col-lg-6">
                            <?= $this->session->flashdata('message'); ?>
                        </div>
                    </div>
                    <form action="<?= site_url('manajemen/detailpelanggaransiswa') ?>" method="post" id="form-data">
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label for="nis">Siswa</label>
                                    <select name="nis" id="nis" class="form-control" required></select>
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label for="tahun">Tahun Akademik</label>
                                    <select name="tahun" id="tahun" class="form-control select2" required>
                                        <?php foreach ($tahun as $v) { ?>
                                            <option value="<?= $v['idtahun'] ?>"><?= $v['namatahun'] ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label for="jenis">Jenis Pelanggaran</label>
                                    <select name="jenis" id="jenis" class="form-control select2" required>
                                        <?php foreach ($jenis as $v) { ?>
                                            <option value="<?= $v['idjenispelanggaran'] ?>"><?= $v['namapelanggaran'] . ' | Skor : ' . $skor[$v['idskor']] ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label for="skor">Skor</label>
                                    <input type="number" name="skor" id="skor" class="form-control" required />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label for="tgl">Tanggal Pelanggaran</label>
                                    <input type="text" name="tgl" id="tgl" class="form-control" required />
                                </div>
                            </div>
                            <div class="col">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                            </div>
                            <div class="col" align="right">
                                <a href="<?= site_url('manajemen') ?>" class="btn btn-secondary">Batal</a>
                                <button type="submit" class="btn btn-success">Simpan</button>
                            </div>
                        </div>
                    </form>
                <?php } ?>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

<script>
    $("#tgl").datepicker({
        format: 'dd-mm-yyyy',
    });
    $('#nis').select2({
        minimumInputLength: 3,
        allowClear: true,
        placeholder: 'Masukkan siswa',
        ajax: {
            dataType: 'json',
            type: 'POST',
            url: '<?= site_url('master/cariSiswa/') ?>',
            delay: 250,
            data: function(params) {
                return {
                    cari: params.term
                }
            },
            processResults: function(data, page) {
                return {
                    results: data
                };
            },
        }
    })
</script>