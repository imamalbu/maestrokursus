<!-- Main Content -->
<div id="content">
    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Page Heading -->
        <h1 class="h3 mb-4 text-gray-800"><?= $profile; ?></h1>
        <div class="card shadow mb-4">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-6">
                        <?= $this->session->flashdata('message'); ?>
                    </div>
                </div>
                <form action="<?= site_url('manajemen/formMapelGuru/' . (isset($key) ? $key : '')) ?>" method="post" id="form-data">
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="kelas">Mata Pelajaran</label>
                                <?php if (isset($key)) { ?>
                                    <input type="text" class="form-control" value="<?= $detail['mapel']['namamapel'] ?>" readonly>
                                <?php } ?>
                                <select name="mapel" id="mapel" class="form-control" <?= isset($key) ? '' : 'required' ?>></select>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label for="tgl">Guru</label>
                                <?php if (isset($key)) { ?>
                                    <input type="text" class="form-control" value="<?= $detail['guru']['namaguru'] ?>" readonly>
                                <?php } ?>
                                <select name="guru" id="guru" class="form-control" <?= isset($key) ? '' : 'required' ?>></select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                        </div>
                        <div class="col" align="bottom">
                            <a href="<?= site_url('manajemen/mapelguru') ?>" class="btn btn-secondary">Batal</a>
                            <button type="submit" class="btn btn-success"><?= $status; ?></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    $('#mapel').select2({
        minimumInputLength: 3,
        allowClear: true,
        placeholder: 'Masukkan mata pelajaran',
        ajax: {
            dataType: 'json',
            type: 'POST',
            url: '<?= site_url('manajemen/cariMapel/') ?>',
            delay: 250,
            data: function(params) {
                return {
                    cari: params.term
                }
            },
            processResults: function(data, page) {
                return {
                    results: data
                };
            },
        }
    });

    $('#guru').select2({
        minimumInputLength: 3,
        allowClear: true,
        placeholder: 'Masukkan Guru',
        ajax: {
            dataType: 'json',
            type: 'POST',
            url: '<?= site_url('manajemen/cariGuru/') ?>',
            delay: 250,
            data: function(params) {
                return {
                    cari: params.term
                }
            },
            processResults: function(data, page) {
                return {
                    results: data
                };
            },
        }
    });
</script>