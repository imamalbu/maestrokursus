    <!-- Main Content -->
    <div id="content">
        <!-- Begin Page Content -->
        <div class="container-fluid">

            <!-- Page Heading -->
            <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>

            <div class="col-md-12">
                <div class="card shadow mb-4">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <?= $this->session->flashdata('message'); ?>
                                <form action="<?= site_url('master/kategori') ?>" method="post" id="form-list">
                                    <?php if (!empty($pembayaran)) : ?>
                                        <table class="table table-hover" id="datatable">
                                            <thead>
                                                <tr>
                                                    <th scope="col">Invoice</th>
                                                    <th scope="col">Nama Peserta</th>
                                                    <th scope="col">Paket Kursus</th>
                                                    <th scope="col">Total</th>
                                                    <th scope="col">Bukti</th>
                                                    <th scope="col">Waktu</th>
                                                    <th scope="col">Status</th>
                                                    <th scope="col">Aksi</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $i = 1; ?>
                                                <?php foreach ($pembayaran as $r) : ?>
                                                    <tr>
                                                        <td><?= $r['invoice']; ?></td>
                                                        <td><?= $r['namapeserta']; ?></td>
                                                        <td><?= $r['namapaketkursus']; ?></td>
                                                        <td><?= toRupiah($r['total']); ?></td>
                                                        <td align="center"><?= empty($r['buktibayar']) ? '<i class="fa fa-times text-danger"></i>' : '<a target="_blank" href="' . base_url('assets/img/buktibayar/') . $r['buktibayar'] . '">' . $r['buktibayar'] . '</a>'; ?></td>
                                                        <td><?= $r['waktu']; ?></td>
                                                        <td><?= $r['status'] == 0 ? 'Belum Bayar' : 'Sudah Bayar'; ?></td>
                                                        <td>
                                                            <?php if ($this->session->userdata('role_id') == 1) : ?>
                                                                <button type="button" data-type="update" class="btn btn-sm btn-success" data-id="<?= $r['iddaftar'] ?>" data-key="<?= $r['iddaftar'] ?>">Update</button>
                                                            <?php endif ?>
                                                            <button type="button" data-type="detail" class="btn btn-sm btn-info" data-key="<?= $r['iddaftar'] ?>" data-id="<?= $r['idpembayaran'] ?>">Detail</button>
                                                        </td>
                                                    </tr>
                                                    <?php $i++; ?>
                                                <?php endforeach; ?>
                                            </tbody>
                                        </table>
                                    <?php else : ?>
                                        <div class="alert alert-warning" role="alert">
                                            <h4 class="alert-heading"><b>Belum ada pembayaran!</b></h4>
                                            <p>Maaf untuk saat ini tidak ada pembayaran yang dilakukan oleh peserta kursus. Jika ada pembayaran maka akan muncul list pembayaran pada halaman ini. Terimakasih.</p>
                                            <hr>
                                            <p class="mb-0">Sistem <?= settingSIM()['app_name'] . ' ' . date('Y') ?></p>
                                        </div>
                                    <?php endif; ?>
                                    <input type="hidden" name="act" id="act">
                                    <input type="hidden" name="key" id="key">
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
        <!-- /.container-fluid -->

    </div>
    <!-- End of Main Content -->
    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">Update Status Pembayaran</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="" method="POST">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="status">Status Pembayaran</label>
                            <select name="status" id="status" class="form-control">
                                <option value="1">Sudah Membayar</option>
                                <option value="0">Belum Membayar</option>
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" data-type="btn-update" data-id="" data-key="" class="btn btn-primary">Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <script>
        $('[data-type=detail]').click(function() {
            var idpembayaran = $(this).attr('data-id');

            location.href = '<?= base_url('pembayaran/detail/') ?>' + idpembayaran;
        });

        $('[data-type=update]').click(function() {
            var modal = $('#exampleModalCenter');
            var id = $(this).attr('data-id');
            var key = $(this).attr('data-key');
            modal.find('[data-type=btn-update]').attr('data-id', id);
            modal.find('[data-type=btn-update]').attr('data-key', key);
            modal.modal();
        });

        $('[data-type=btn-update]').click(function() {
            var id = $(this).attr('data-id');
            var key = $(this).attr('data-key');
            var status = $('#exampleModalCenter').find('#status').val();

            location.href = '<?= base_url('pembayaran/updateStatusPembayaran/') ?>' + status + '/' + id + '/' + key;
        });
    </script>