<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <?php if (!empty($this->session->userdata('idowner'))) : ?>
        <title><?= $title . ' | ' . getCourse()->row_array()['namakursus'] ?></title>
    <?php else : ?>
        <title><?= $title . ' | ' . settingSIM()['app_name'] ?></title>
    <?php endif ?>

    <!-- Custom fonts for this template-->
    <link href="<?= base_url('assets/v2/'); ?>vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="<?= base_url('assets/v2/'); ?>css/sb-admin-2.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?= base_url('assets/v2/'); ?>vendor/sweetalert2/dist/sweetalert2.min.css">
    <link rel="stylesheet" href="<?= base_url('assets/v2/'); ?>vendor/datatables110/datatables.min.css">
    <link rel="stylesheet" href="<?= base_url('assets/v2/'); ?>vendor/select2/dist/css/select2.min.css">
    <link rel="stylesheet" href="<?= base_url('assets/v2/'); ?>vendor/datepicker/datepicker3.css">
    <!-- <link rel="stylesheet" href="<?= base_url('assets/v2/'); ?>vendor/jquery-ui/jquery-ui.min.css"> -->
    <script src="<?= base_url('assets/v2/'); ?>vendor/jquery/jquery.min.js"></script>
    <!-- <script src="<?= base_url('assets/v2/'); ?>vendor/jquery-ui/jquery-ui.min.js"></script> -->
    <script src="<?= base_url('assets/v2/'); ?>js/jquery.ajax.js"></script>
    <script src="<?= base_url('assets/v2/'); ?>vendor/select2/dist/js/select2.full.min.js"></script>
    <script src="<?= base_url('assets/v2/'); ?>vendor/sweetalert2/dist/sweetalert2.min.js"></script>
    <script src="<?= base_url('assets/v2/'); ?>vendor/datatables110/datatables.min.js"></script>
    <script src="<?= base_url('assets/v2/'); ?>vendor/datepicker/bootstrap-datepicker.js"></script>
    <script src="<?php echo base_url('assets/v2/') ?>vendor/tinymce/tinymce.min.js"></script>
</head>

<body id="page-top">

    <!--Page Wrapper-->
    <div id="wrapper">