<!-- Main Content -->
<div id="content">
    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Page Heading -->
        <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>

        <div class="card shadow mb-4">
            <div class="card-body">
                <?php if ($is_crud == 0 || $is_crud == $this->session->userdata('role_id')) : ?>
                    <a class="btn btn-primary mb-3" href="<?= $menu . '/create' ?>">Tambah <?= $title; ?></a>
                <?php endif; ?>

                <div class="row">
                    <div class="col-md-12">
                        <?php if (validation_errors()) : ?>
                            <div class="alert alert-danger" role="alert">
                                <?= validation_errors(); ?>
                            </div>
                        <?php endif; ?>
                        <?= $this->session->flashdata('message'); ?>
                        <?= $this->session->flashdata('delete'); ?>
                        <form method="post" id="form_list">
                            <table class="table table-hover" id="datatable">
                                <thead>
                                    <tr>
                                        <?php foreach ($a_kolom as $key => $val) {
                                            if ($val['kolom'] == ':no') { ?>
                                                <th scope="col">No</th>
                                            <?php } else { ?>
                                                <?php if (!isset($val['is_tampil']) || $val['is_tampil'] == true) : ?>
                                                    <th scope="col"><?= $val['label'] ?></th>
                                                <?php endif ?>
                                            <?php } ?>
                                        <?php } ?>
                                        <?php if ($is_crud == 0 || $is_crud == $this->session->userdata('role_id')) : ?>
                                            <th scope="col">Action</th>
                                        <?php endif; ?>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1; ?>
                                    <?php $num_rows = count($a_data) ?>
                                    <?php foreach ($a_data as $row) { ?>
                                        <tr>
                                            <?php
                                            foreach ($a_kolom as $key => $val) {
                                                if ($val['kolom'] == ':no') { ?>
                                                    <td scope="row"><?= $i++ ?></td>
                                                <?php } else if (isset($val['type']) && $val['type'] == 'S') {
                                                    $option = $val['option'];
                                                ?>
                                                    <td>
                                                        <?php if (!isset($val['is_tampil']) || $val['is_tampil'] == true) : ?>
                                                            <?php if (isset($val['view_type']) && isset($val['color'])) : ?>
                                                                <span class="<?= $val['view_type'] . " " . $val['view_type'] . "-" . $val['color'][$row[$val['kolom']]] ?>"><?= $option[$row[$val['kolom']]] ?></span>
                                                            <?php else : ?>
                                                                <?= $option[$row[$val['kolom']]] ?>
                                                            <?php endif ?>
                                                        <?php endif; ?>
                                                    </td>
                                                <?php } else { ?>
                                                    <?php if (!isset($val['is_tampil']) || $val['is_tampil'] == true) : ?>
                                                        <?php if ($val['type'] != 'F') : ?>
                                                            <td><?= $val['set_currency'] ? toRupiah($row[$val['kolom']]) : $row[$val['kolom']]; ?></td>
                                                        <?php else : ?>
                                                            <td><a href="<?= base_url(substr($val['path'], 1) . $row[$val['kolom']]) ?>" target="_blank"><?= $row[$val['kolom']]; ?></a></td>
                                                        <?php endif; ?>
                                                    <?php endif; ?>
                                                <?php } ?>
                                            <?php } ?>
                                            <td>
                                                <?php if (isset($a_buttonlinear)) {
                                                    foreach ($a_buttonlinear as $k => $v) { ?>
                                                        <button type="button" data-type="<?= $v['type'] ?>" data-id="<?= $row[$primary]; ?>" class="btn btn-<?= $v['class'] ?>" <?= $v['add'] ?>><?= $v['label'] ?></button>
                                                <?php }
                                                } ?>
                                                <?php if ($is_crud == 0 || $is_crud == $this->session->userdata('role_id')) : ?>
                                                    <button type="button" data-type="edit" data-id="<?= $row[$primary]; ?>" class="btn btn-sm btn-info d-inline">Ubah</button>
                                                    <button type="button" data-type="delete" data-id="<?= $row[$primary]; ?>" class="btn btn-sm btn-danger">Hapus</button>
                                                <?php endif; ?>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                            <input type="hidden" name="act" id="act">
                            <input type="hidden" name="key" id="key">
                        </form>
                    </div>
                    <?= $this->pagination->create_links(); ?>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
</div>
<!-- End of Main Content -->
<script>
    $(function() {
        $('[data-type="edit"]').click(function() {
            location.href = '<?= site_url($parent . '/' . $menu . '/detail/') ?>' + $(this).attr('data-id');
        })
        $('[data-type="delete"]').click(function() {
            var id = $(this).attr('data-id');
            bootbox.confirm("Apakah anda ingin menghapus data?", function(result) {
                if (result) {
                    $('#form_list #key').val(id);
                    $('#form_list #act').val('delete');
                    $('#form_list').submit();
                }
            })
        })
    })

    var num_rows = <?= $num_rows ?>;

    if (num_rows < 10) {
        $('#datatable').DataTable();
    }
</script>