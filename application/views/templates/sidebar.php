<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <?php
    $setting = settingSIM();

    ?>
    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?= site_url() ?>">
        <div class="sidebar-brand-icon">
            <i class="fas fa-graduation-cap"></i>
        </div>
        <div class="sidebar-brand-text mx-2 mt-2">
            <?php if (!empty($this->session->userdata('idowner'))) : ?>
                <?= getCourse()->row_array()['namakursus']; ?>
            <?php else : ?>
                <?= $setting['app_name'] ?>
            <?php endif ?>
        </div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <li class="nav-item <?= $title == "Dashboard" ? 'active' : '' ?>">
        <a class="nav-link" href="<?= site_url('home') ?>">
            <i class="fas fa-home"></i>
            <span>Dashboard</span>
        </a>
    </li>

    <!-- Query menu -->
    <?php
    $role_id = $this->session->userdata('role_id');
    $menu = getMenu($role_id);
    ?>

    <!-- LOOPING MENU -->
    <?php
    $active = '';
    foreach ($menu as $m) {
        foreach ($m['submenu'] as $sm) {
            if ($title == $sm['title'])
                $active = $m['id'];
        } ?>
        <!-- Nav Item - Pages Collapse Menu -->
        <li class="nav-item">
            <a class="nav-link" href="#" data-toggle="collapse" data-target="#menu<?= $m['id'] ?>" aria-expanded="true" aria-controls="collapseMenu<?= $m['id'] ?>">
                <i class="<?= $m['icon']; ?>"></i>
                <span><?= $m['menu']; ?></span>
            </a>
            <div id="menu<?= $m['id'] ?>" class="collapse <?= $active == $m['id'] ? 'show' : '' ?>" aria-labelledby="headingPages" data-parent="#accordionSidebar">
                <div class="bg-white py-3 collapse-inner rounded">
                    <!-- SIAPKAN SUBMENU MENU -->
                    <?php foreach ($m['submenu'] as $sm) { ?>
                        <!-- Nav Item - Dashboard -->
                        <?php if ($sm['role_id'] == $this->session->userdata('role_id') || empty($sm['role_id']) || $sm['role_id'] == 0) : ?>
                            <?php if($sm['read_mode'] == 0): ?>
                                <a class="collapse-item <?= $title == $sm['title'] ? 'active' : '' ?>" href="<?= base_url($sm['url']); ?>"><?= $sm['title'] ?></a>
                            <?php else:?>
                                <a class="collapse-item <?= $title == $sm['title'] ? 'active' : '' ?>" href="<?= base_url($sm['url']); ?>"><?= $sm['title'] ?><span class="badge badge-danger ml-2"><?= countUnread($sm['read_table']) > 0 ?countUnread($sm['read_table']) : "" ; ?></span></a>
                            <?php endif;?>
                        <?php endif ?>
                    <?php } ?>
                </div>
            </div>

            <!-- Divider -->
        </li>

    <?php } ?>

    <!-- Nav Item - Charts -->
    <li class="nav-item">
        <a class="nav-link" href="#" data-toggle="modal" data-target="#logoutModal" data-toggle="modal" data-target="#logoutModal">
            <i class="fas fa-sign-out-alt"></i>
            <span>Logout</span></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>
<!-- End of Sidebar -->